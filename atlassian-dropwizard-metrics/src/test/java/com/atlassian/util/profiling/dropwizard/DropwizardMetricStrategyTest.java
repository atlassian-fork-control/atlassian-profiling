package com.atlassian.util.profiling.dropwizard;

import com.atlassian.util.profiling.Histogram;
import com.atlassian.util.profiling.Metrics;
import com.atlassian.util.profiling.MetricTimer;
import com.atlassian.util.profiling.MetricsConfiguration;
import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.Timer;
import com.atlassian.util.profiling.Timers;
import com.codahale.metrics.MetricRegistry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DropwizardMetricStrategyTest {

    private MetricsConfiguration configuration;
    private MetricRegistry registry;
    private DropwizardMetricStrategy strategy;
    private boolean wasEnabled;

    @Before
    public void setup() {
        registry = new MetricRegistry();
        strategy = new DropwizardMetricStrategy(registry);

        configuration = Metrics.getConfiguration();
        wasEnabled = configuration.isEnabled();
        configuration.setEnabled(true);
        StrategiesRegistry.addMetricStrategy(strategy);
    }

    @After
    public void tearDown() {
        StrategiesRegistry.removeMetricStrategy(strategy);
        configuration.setEnabled(wasEnabled);
    }

    @Test
    public void testDisabledMetricWorksWhenEnabled() {
        configuration.setEnabled(false);

        MetricTimer metric = Metrics.timer("my.metric");
        com.codahale.metrics.Timer dwTimer = registry.timer("my.metric");

        assertEquals(0L, dwTimer.getCount());

        metric.start().close();

        assertEquals(0L, dwTimer.getCount());

        configuration.setEnabled(true);
        metric.start().close();

        assertEquals(1L, dwTimer.getCount());
    }

    @Test
    public void testEnabledMetricNoopsWhenDisabled() {
        MetricTimer metric = Metrics.timer("my.metric");
        com.codahale.metrics.Timer dwTimer = registry.timer("my.metric");

        assertEquals(0L, dwTimer.getCount());

        // verify that the metric is enabled
        metric.start().close();
        assertEquals(1L, dwTimer.getCount());

        // disable the strategy and verify that interacting with the metric does nothing
        configuration.setEnabled(false);
        metric.start().close();

        assertEquals(1L, dwTimer.getCount());

        // re-enable and verify that the metric is enabled too
        configuration.setEnabled(true);
        metric.start().close();

        assertEquals(2L, dwTimer.getCount());
    }

    @Test
    public void testHistogram() {
        Histogram histogram = Metrics.histogram("histo");
        com.codahale.metrics.Histogram dwHistogram = registry.histogram("histo");

        assertEquals(0, dwHistogram.getCount());

        histogram.update(1L);
        assertEquals(1, dwHistogram.getCount());

        histogram.update(2L);
        assertEquals(2, dwHistogram.getCount());

        histogram.update(3L);
        assertEquals(3, dwHistogram.getCount());
    }

    @Test
    public void testMaxNumberOfMetricsIsRespected() {
        StrategiesRegistry.removeMetricStrategy(strategy);

        strategy = new DropwizardMetricStrategy.Builder(registry)
                .maxNumberOfMetrics(2)
                .build();
        StrategiesRegistry.addMetricStrategy(strategy);

        Metrics.startTimer("test.metric.1").close();
        Metrics.startTimer("test.metric.2").close();
        Metrics.startTimer("test.metric.3").close();
        Metrics.startTimer("test.metric.1").close();

        com.codahale.metrics.Timer dwTimer1 = registry.getTimers().get("test.metric.1");
        assertNotNull(dwTimer1);
        assertEquals(2L, dwTimer1.getCount());
        com.codahale.metrics.Timer dwTimer2 = registry.getTimers().get("test.metric.2");
        assertNotNull(dwTimer2);
        assertEquals(1L, dwTimer2.getCount());
        assertNull(registry.getTimers().get("test.metric.3"));
    }

    @Test
    public void testMetric() {
        MetricTimer metric = Metrics.timer("test.metric");
        com.codahale.metrics.Timer dwTimer = registry.timer("test.metric");

        assertEquals(0L, dwTimer.getCount());

        metric.start().close();
        assertEquals(1L, dwTimer.getCount());

        metric.start().close();
        assertEquals(2L, dwTimer.getCount());
    }

    @Test
    public void testTimerWithMetric() {
        Timer timer = Timers.timerWithMetric("test.metric");
        com.codahale.metrics.Timer dwTimer = registry.timer("test.metric");

        assertEquals(0L, dwTimer.getCount());

        timer.start().close();
        assertEquals(1L, dwTimer.getCount());
    }

    @Test
    public void testTimerWithMetricDifferentMetricName() {
        Timer timer = Timers.timerWithMetric("something something", "test.metric");
        com.codahale.metrics.Timer dwTimer = registry.timer("test.metric");

        assertEquals(0L, dwTimer.getCount());

        timer.start().close();
        assertEquals(1L, dwTimer.getCount());
    }
}