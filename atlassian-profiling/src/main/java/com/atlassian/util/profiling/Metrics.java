package com.atlassian.util.profiling;

import com.atlassian.util.profiling.strategy.MetricStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Duration;
import java.util.Collection;

import static com.atlassian.util.profiling.MetricKey.metricKey;
import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Objects.requireNonNull;

/**
 * Factory and utility methods for create {@link MetricTimer} and {@link Histogram}.
 *
 * <pre>
 *     try (Ticker ignored = Metrics.startTimer("my-timer")) {
 *         // monitored code block here
 *     }
 * </pre>
 *
 * @since 3.0
 */
@ParametersAreNonnullByDefault
public class Metrics {
    private static final MetricsConfiguration CONFIGURATION = new MetricsConfiguration();

    private Metrics() {
        throw new UnsupportedOperationException("Metrics is an utility class and should not be instantiated");
    }

    @Nonnull
    public static MetricsConfiguration getConfiguration() {
        return CONFIGURATION;
    }

    @Nonnull
    public static Histogram histogram(String name) {
        return new DefaultHistogram(metricKey(name));
    }

    /**
     * @since 3.4
     */
    @Nonnull
    public static Histogram histogram(String name, Collection<MetricTag> tags) {
        return new DefaultHistogram(metricKey(name, tags));
    }

    /**
     * @since 3.4
     */
    @Nonnull
    public static Histogram histogram(String name, MetricTag... tags) {
        return new DefaultHistogram(metricKey(name, tags));
    }

    @Nonnull
    public static MetricTimer timer(String name) {
        return new DefaultMetricTimer(metricKey(name));
    }

    /**
     * @since 3.4
     */
    @Nonnull
    public static MetricTimer timer(String name, Collection<MetricTag> tags) {
        return new DefaultMetricTimer(metricKey(name, tags));
    }

    /**
     * @since 3.4
     */
    @Nonnull
    public static MetricTimer timer(String name, MetricTag... tags) {
        return new DefaultMetricTimer(metricKey(name, tags));
    }

    @Nonnull
    public static Ticker startTimer(String name) {
        return startTimer(name, emptySet());
    }

    /**
     * @since 3.4
     */
    @Nonnull
    public static Ticker startTimer(String name, MetricTag... tags) {
        return startTimer(name, asList(tags));
    }

    /**
     * @since 3.4
     */
    @Nonnull
    public static Ticker startTimer(String name, Collection<MetricTag> tags) {
        if (CONFIGURATION.isEnabled()) {
            return timer(name, tags).start();
        }
        return Ticker.NO_OP;
    }

    @ParametersAreNonnullByDefault
    private static class DefaultMetricTimer implements MetricTimer {
        private static final Logger log = LoggerFactory.getLogger(DefaultMetricTimer.class);

        private final MetricKey metricKey;

        DefaultMetricTimer(MetricKey name) {
            this.metricKey = requireNonNull(name, "metricKey");
        }

        @Nonnull
        @Override
        public Ticker start() {
            Collection<MetricStrategy> metricStrategies = StrategiesRegistry.getMetricStrategies();
            if (!getConfiguration().isEnabled() || metricStrategies.isEmpty()) {
                return Ticker.NO_OP;
            }

            CompositeTicker compositeTicker = null;
            for (MetricStrategy strategy : metricStrategies) {
                try {
                    compositeTicker = Tickers.addTicker(strategy.startTimer(metricKey), compositeTicker);
                } catch (RuntimeException e) {
                    log.warn("Failed to start metric trace for {}", metricKey, e);
                }
            }

            return compositeTicker == null ? Ticker.NO_OP : compositeTicker;
        }

        @Override
        public void update(Duration time) {
            if (getConfiguration().isEnabled()) {
                requireNonNull(time, "time");
                for (MetricStrategy strategy : StrategiesRegistry.getMetricStrategies()) {
                    try {
                        strategy.updateTimer(metricKey, time);
                    } catch (RuntimeException e) {
                        log.warn("Failed to update metric for {}", metricKey, e);
                    }
                }
            }
        }
    }

    private static class DefaultHistogram implements Histogram {
        private static final Logger log = LoggerFactory.getLogger(DefaultHistogram.class);

        private final MetricKey metricKey;

        private DefaultHistogram(MetricKey metricKey) {
            this.metricKey = requireNonNull(metricKey, "metricKey");
        }

        @Override
        public void update(long value) {
            if (getConfiguration().isEnabled()) {
                for (MetricStrategy strategy : StrategiesRegistry.getMetricStrategies()) {
                    try {
                        strategy.updateHistogram(metricKey, value);
                    } catch (RuntimeException e) {
                        log.warn("Failed to update histogram for {}", metricKey, e);
                    }
                }
            }
        }
    }
}
