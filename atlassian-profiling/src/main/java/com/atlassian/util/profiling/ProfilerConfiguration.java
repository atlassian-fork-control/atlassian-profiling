package com.atlassian.util.profiling;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;

import static com.atlassian.util.profiling.ProfilingConstants.ACTIVATE_MEMORY_PROPERTY;
import static com.atlassian.util.profiling.ProfilingConstants.ACTIVATE_PROPERTY;
import static com.atlassian.util.profiling.ProfilingConstants.DEFAULT_ACTIVATE_MEMORY_PROPERTY;
import static com.atlassian.util.profiling.ProfilingConstants.DEFAULT_ACTIVATE_PROPERTY;
import static com.atlassian.util.profiling.ProfilingConstants.DEFAULT_MAX_FRAME_COUNT;
import static com.atlassian.util.profiling.ProfilingConstants.DEFAULT_MAX_FRAME_LENGTH;
import static com.atlassian.util.profiling.ProfilingConstants.DEFAULT_MIN_TIME;
import static com.atlassian.util.profiling.ProfilingConstants.DEFAULT_MIN_TOTAL_TIME;
import static com.atlassian.util.profiling.ProfilingConstants.MAX_FRAME_COUNT;
import static com.atlassian.util.profiling.ProfilingConstants.MAX_FRAME_LENGTH;
import static com.atlassian.util.profiling.ProfilingConstants.MIN_TIME;
import static com.atlassian.util.profiling.ProfilingConstants.MIN_TOTAL_TIME;
import static java.util.Objects.requireNonNull;

/**
 * Configuration that applies to {@link com.atlassian.util.profiling.strategy.ProfilerStrategy}
 *
 * @since 3.0
 */
public class ProfilerConfiguration {

    private final ThreadLocal<Boolean> enabledForThread;

    private int maxFramesPerTrace;
    private int maxFrameNameLength;
    private long minFrameTimeNanos;
    private long minTraceTimeNanos;
    private boolean enabled;
    private boolean memoryProfiled;

    public ProfilerConfiguration() {
        enabledForThread = new ThreadLocal<>();

        enabled = "true".equalsIgnoreCase(System.getProperty(ACTIVATE_PROPERTY, DEFAULT_ACTIVATE_PROPERTY));
        maxFrameNameLength = Integer.getInteger(MAX_FRAME_LENGTH, DEFAULT_MAX_FRAME_LENGTH);
        maxFramesPerTrace = Integer.getInteger(MAX_FRAME_COUNT, DEFAULT_MAX_FRAME_COUNT);
        memoryProfiled = "true".equalsIgnoreCase(System.getProperty(ACTIVATE_MEMORY_PROPERTY, DEFAULT_ACTIVATE_MEMORY_PROPERTY));
        minFrameTimeNanos = TimeUnit.MILLISECONDS.toNanos(Long.getLong(MIN_TIME, DEFAULT_MIN_TIME));
        minTraceTimeNanos = TimeUnit.MILLISECONDS.toNanos(Long.getLong(MIN_TOTAL_TIME, DEFAULT_MIN_TOTAL_TIME));
    }

    /**
     * @return a ticker that can be closed to end the temporary enabling of profiling on the current thread
     */
    @Nonnull
    public Ticker enableForThread() {
        enabledForThread.set(true);
        return enabledForThread::remove;
    }

    /**
     * @return the maximum number of frames to include in a profiling trace
     */
    public int getMaxFramesPerTrace() {
        return maxFramesPerTrace;
    }

    /**
     * @return the maximum number of characters in a profiling frame name. Longer names will be truncated.
     */
    public int getMaxFrameNameLength() {
        return maxFrameNameLength;
    }

    /**
     * @param timeUnit the requested time unit
     * @return the minimum time a frame should take to be included in a profiling trace. Frames that take less than
     *         this amount of time will be suppressed from the trace.
     */
    public long getMinFrameTime(@Nonnull TimeUnit timeUnit) {
        return requireNonNull(timeUnit, "timeUnit").convert(minFrameTimeNanos, TimeUnit.NANOSECONDS);
    }

    /**
     * @param timeUnit the requested time unit
     * @return the minimum time a profiling trace should take. Traces that take less than this amount of time will
     *         be pruned from profiling output.
     */
    public long getMinTraceTime(@Nonnull TimeUnit timeUnit) {
        return requireNonNull(timeUnit, "timeUnit").convert(minTraceTimeNanos, TimeUnit.NANOSECONDS);
    }

    /**
     * @return whether profiling is enabled
     */
    public boolean isEnabled() {
        if (enabled || enabledForThread.get() == Boolean.TRUE) {
            return true;
        }

        enabledForThread.remove();
        return false;
    }

    /**
     * @return whether profiling should include memory usage
     */
    public boolean isMemoryProfilingEnabled() {
        return memoryProfiled;
    }

    /**
     * @param value whether profiling should be enabled
     */
    public void setEnabled(boolean value) {
        enabled = value;
    }

    /**
     * @param value the maximum number of frames to include in a profiling trace
     */
    public void setMaxFramesPerTrace(int value) {
        maxFramesPerTrace = value;
    }

    /**
     * @param value the maximum number of characters in a profiling frame name. Longer names will be truncated.
     */
    public void setMaxFrameNameLength(int value) {
        maxFrameNameLength = value;
    }

    /**
     * @param value whether profiling should include memory usage
     */
    public void setMemoryProfilingEnabled(boolean value) {
        memoryProfiled = value;
    }

    /**
     * @param value the minimum time a profiling trace should take. Traces that take less than this amount of time will
     *              be pruned from profiling output.
     * @param timeUnit the time unit of {@code value}
     */
    public void setMinFrameTime(long value, @Nonnull TimeUnit timeUnit) {
        minFrameTimeNanos = timeUnit.toNanos(value);
    }

    /**
     * @param value the minimum time a profiling trace should take. Traces that take less than this amount of time will
     *              be pruned from profiling output.
     * @param timeUnit the time unit of {@code value}
     */
    public void setMinTraceTime(long value, @Nonnull TimeUnit timeUnit) {
        minTraceTimeNanos = timeUnit.toNanos(value);
    }
}
