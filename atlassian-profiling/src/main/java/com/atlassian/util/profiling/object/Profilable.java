package com.atlassian.util.profiling.object;

public interface Profilable {
    Object profile() throws Exception;
}
