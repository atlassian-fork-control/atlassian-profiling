package com.atlassian.util.profiling.strategy.impl;

import com.atlassian.util.profiling.ProfilerConfiguration;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.Timers;
import com.atlassian.util.profiling.UtilTimerLogger;
import com.atlassian.util.profiling.strategy.ProfilingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

/**
 * Default profiling strategy, which is included into {@link com.atlassian.util.profiling.UtilTimerStack} by default.
 * Before version 2.0 that was the only one available profiling functionality of the
 * {@link com.atlassian.util.profiling.UtilTimerStack}
 *
 * @since 2.0
 * @deprecated in 3.0 for removal in 4.0. Use {@link StackProfilerStrategy} and
 *             {@link Timers} instead.
 */
@Deprecated
public class StackProfilingStrategy implements ProfilingStrategy {

    private static final Logger log = LoggerFactory.getLogger(Timers.class);
    private static final UtilTimerLogger DEFAULT_LOGGER = log::debug;

    private final ProfilerConfiguration config;
    private final StackProfilerStrategy delegate;

    private UtilTimerLogger logger;

    public StackProfilingStrategy() {
        this(new StackProfilerStrategy());
    }

    public StackProfilingStrategy(@Nonnull StackProfilerStrategy delegate) {
        this.delegate = requireNonNull(delegate, "delegate");
        this.logger = DEFAULT_LOGGER;

        config = Timers.getConfiguration();
        delegate.setLogger(log);
    }

    public UtilTimerLogger getLogger() {
        return logger;
    }

    public int getMaxFrameCount() {
        return config.getMaxFramesPerTrace();
    }

    public long getMinTime() {
        return config.getMinFrameTime(TimeUnit.MILLISECONDS);
    }

    public long getMinTotalTime() {
        return config.getMinTraceTime(TimeUnit.MILLISECONDS);
    }

    @Override
    public boolean isEnabled() {
        return config.isEnabled();
    }

    public boolean isProfileMemory() {
        return config.isMemoryProfilingEnabled();
    }

    public void setConfiguredMaxFrameCount(int value) {
        config.setMaxFramesPerTrace(value);
    }

    public void setConfiguredMinTime(long value) {
        config.setMinFrameTime(value, TimeUnit.MILLISECONDS);
    }

    public void setConfiguredMinTotalTime(long value) {
        config.setMinTraceTime(value, TimeUnit.MILLISECONDS);
    }

    public void setEnabled(boolean value) {
        config.setEnabled(value);
    }

    public void setLogger(@Nonnull UtilTimerLogger logger) {
        this.logger = requireNonNull(logger, "logger");

        // StackProfilerStrategy no longer uses an slf4j Logger instead of a UtilTimerLogger. Register an adapted
        // version of logger with the delegate that translates the slf4j calls to the UtilTimerLogger
        Logger logAdapter = (Logger) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] {Logger.class},
                new UtilTimerLoggerAdapter(logger));
        delegate.setLogger(logAdapter);
    }

    public void setProfileMemoryFlag(boolean value) {
        config.setMemoryProfilingEnabled(value);
    }

    @Override
    public void start(String name) {
        delegate.start(name);
    }

    @Override
    public void stop(String name) {
        Ticker ticker = delegate.getTicker(name);
        if (ticker != null) {
            ticker.close();
        }
    }

    private static class UtilTimerLoggerAdapter implements InvocationHandler {

        private final UtilTimerLogger logger;

        private UtilTimerLoggerAdapter(UtilTimerLogger logger) {
            this.logger = logger;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) {
            // only handle the 2 slf4j Logger methods that StackProfilerStrategy calls: isDebugEnabled and debug(String)
            if ("isDebugEnabled".equals(method.getName())) {
                return true;
            }
            if ("debug".equals(method.getName()) && args.length > 0) {
                logger.log((String) args[0]);
            }
            return null;
        }
    }
}
