package com.atlassian.util.profiling;

import static com.atlassian.util.profiling.ProfilingConstants.ACTIVATE_METRICS_PROPERTY;
import static com.atlassian.util.profiling.ProfilingConstants.DEFAULT_ACTIVATE_METRICS_PROPERTY;

/**
 * Configuration that applies to {@link com.atlassian.util.profiling.strategy.MetricStrategy}
 *
 * @since 3.0
 */
public class MetricsConfiguration {

    private boolean enabled;

    public MetricsConfiguration() {
        enabled = "true".equalsIgnoreCase(System.getProperty(ACTIVATE_METRICS_PROPERTY, DEFAULT_ACTIVATE_METRICS_PROPERTY));
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean value) {
        enabled = value;
    }
}
