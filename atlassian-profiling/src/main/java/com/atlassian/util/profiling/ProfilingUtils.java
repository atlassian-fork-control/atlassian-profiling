package com.atlassian.util.profiling;

/**
 * @deprecated in 3.0 for removal in 4.0
 */
@Deprecated
public class ProfilingUtils {
    /**
     * Get just the name of the class (without the package name)
     *
     * @param clazz class
     * @return class name
     */
    public static String getJustClassName(Class clazz) {
        return getJustClassName(clazz.getName());
    }

    /**
     * Get just the name of the class (without the package name)
     *
     * @param name class name
     * @return class name
     */
    public static String getJustClassName(String name) {
        if (name.indexOf(".") >= 0) {
            name = name.substring(name.lastIndexOf(".") + 1);
        }

        return name;
    }
}
