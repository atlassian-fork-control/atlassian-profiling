package com.atlassian.util.profiling;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class CompositeTickerTest {

    @Test
    public void testAdd() {
        CompositeTicker composite = new CompositeTicker();
        Ticker ticker1 = mock(Ticker.class);
        Ticker ticker2 = mock(Ticker.class);

        composite.add(ticker1);
        composite.add(ticker2);

        composite.close();
        verify(ticker1).close();
        verify(ticker2).close();
    }

    @Test
    public void testAddThroughConstructorAndAddMethod() {
        Ticker ticker1 = mock(Ticker.class);
        Ticker ticker2 = mock(Ticker.class);

        CompositeTicker composite = new CompositeTicker(ticker1);
        composite.add(ticker2);

        composite.close();

        verify(ticker1).close();
        verify(ticker2).close();
    }

    @Test
    public void testClosesOnce() {
        Ticker ticker = mock(Ticker.class);
        CompositeTicker composite = new CompositeTicker(ticker);

        composite.close();
        composite.close();

        verify(ticker).close();
    }

    @Test
    public void testEmpty() {
        CompositeTicker ticker = new CompositeTicker();

        ticker.close(); // should not fail
    }
}