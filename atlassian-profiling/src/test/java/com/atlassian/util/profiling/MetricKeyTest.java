package com.atlassian.util.profiling;

import org.junit.Test;

import static com.atlassian.util.profiling.MetricKey.metricKey;
import static java.util.Collections.emptySet;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class MetricKeyTest {

    @Test
    public void testTagFormatting() {
        final MetricKey metricKey = metricKey("mymetric",
                                                        MetricTag.of("key1", "value1"),
                                                        MetricTag.of("key2", "value2"));

        assertThat(metricKey.toString(), equalTo("mymetric[key1=value1,key2=value2]"));
    }

    @Test
    public void testNoTagsFormatting() {
        final MetricKey metricKey = metricKey("mymetric", emptySet());

        assertThat(metricKey.toString(), equalTo("mymetric"));
    }
}