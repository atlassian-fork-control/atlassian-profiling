package com.atlassian.util.profiling.object;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.util.profiling.Timers.getConfiguration;
import static org.junit.Assert.assertTrue;

/**
 * Tests for ObjectProfiler.
 */
public class ObjectProfilerTest {
    private boolean wasActive;

    @Before
    public void setup() {
        wasActive = getConfiguration().isEnabled();
        getConfiguration().setEnabled(true);
    }

    @After
    public void tearDown() {
        getConfiguration().setEnabled(wasActive);
    }

    @Test
    public void testProfileMethodThatReturnsInterface() {
        ServiceImpl svcImpl = new ServiceImpl();
        Service svc = (Service) ObjectProfiler.getProfiledObject(Service.class, svcImpl);

        B b1 = (B) svcImpl.foo(new BImpl()); // works with Impl
        B b2 = (B) svc.foo(new BImpl());     // proxy shouldn't break the above behaviour
        assertTrue(Proxy.isProxyClass(b2.getClass()));
    }

    @Test
    public void testClassThatImplementsAnInterfaceDoublyShouldBeProxiable() {
        Service svc = (Service) ObjectProfiler.getProfiledObject(Service.class, new ServiceImpl());

        Map map = svc.getMap(); // JRA-23098: shouldn't throw IllegalArgumentException
        assertTrue(Proxy.isProxyClass(map.getClass()));
    }

    interface A {
    }

    interface B extends A {
    }

    interface Service {
        // returns A
        A foo(A a);

        Map getMap();
    }

    static class AImpl implements A {
    }

    static class BImpl implements B {
    }

    static class ServiceImpl implements Service {
        public A foo(A a) {
            return a;
        }

        public Map getMap() {
            return new M();
        }
    }

    static class M extends HashMap implements Map {
        // M doubly-implements java.util.Map
    }
}
