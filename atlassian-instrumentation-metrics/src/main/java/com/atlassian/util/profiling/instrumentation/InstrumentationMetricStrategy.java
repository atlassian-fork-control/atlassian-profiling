package com.atlassian.util.profiling.instrumentation;

import com.atlassian.instrumentation.InstrumentRegistry;
import com.atlassian.instrumentation.operations.OpTimer;
import com.atlassian.util.profiling.MetricsConfiguration;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import javax.annotation.Nonnull;

import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

/**
 * Simple {@link MetricStrategy} that delegates to matching {@link OpTimer timers} from an atlassian-instrumentation
 * {@link InstrumentRegistry}
 */
public class InstrumentationMetricStrategy implements MetricStrategy {

    private final InstrumentRegistry registry;

    public InstrumentationMetricStrategy(@Nonnull InstrumentRegistry registry) {
        this.registry = requireNonNull(registry, "registry");
    }

    @Override
    public void setConfiguration(@Nonnull MetricsConfiguration configuration) {
        // no-op
    }

    @Nonnull
    @Override
    public Ticker startTimer(@Nonnull String metricName) {
        OpTimer timer = registry.pullTimer(metricName);
        return timer::end;
    }

    @Override
    public void updateHistogram(@Nonnull String metricName, long value) {
        //no-op since atlassian-instrumentation currently does not have a histogram implementation; Gauge doesn't
        //track statistics (and does not allow us to atomically set a value), and OpCounter is purely time-based,
        //which makes it a bad fit for calculating the distribution of things like queue lengths, connections in
        //use, etc.
    }

    @Override
    public void updateTimer(@Nonnull String metricName, long time, @Nonnull TimeUnit timeUnit) {
        registry.pullTimer(metricName).endWithTime(timeUnit.toMillis(time));
    }
}
